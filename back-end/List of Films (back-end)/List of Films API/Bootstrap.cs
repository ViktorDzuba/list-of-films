using AutoMapper;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System.Web.Http;

namespace List_of_Films_API
{
  public static class Bootstrap
  {
    internal static void ConfigureDependencies()
    {
      ConfigureWindsor(GlobalConfiguration.Configuration);
    }
    public static void ConfigureWindsor(HttpConfiguration configuration)
    {
      var container = new WindsorContainer();
      container.Install(FromAssembly.This());
      container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
      var dependencyResolver = new WindsorDependencyResolver(container);
      configuration.DependencyResolver = dependencyResolver;
    }

    public static MapperConfiguration InitializeAutoMapper()
    {
      MapperConfiguration config = new MapperConfiguration(cfg =>
      {
        cfg.AddProfile(new ApplicationProfile());  //mapping between Web and Business layer objects
      });

      return config;
    }

  }
}
