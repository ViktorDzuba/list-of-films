using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ListOfFilms.Repository.Interfaces;
using ListOfFilms.Repository;
using System.Web.Http.Controllers;
using ListOfFilms.Business;
using ListOfFilms.Business.Interfaces;
using AutoMapper;
using System.Linq;

namespace List_of_Films_API
{
  public class ProjectInstaller : IWindsorInstaller
  {
    public void Install(IWindsorContainer container, IConfigurationStore store)
    {

      container.Register(Classes.FromThisAssembly().BasedOn<IHttpController>().LifestyleTransient());
      container.Register(
        Component.For<IFilmRepository>().ImplementedBy<FilmRepository>(),
        Component.For<IFilmManager>().ImplementedBy<FilmManager>(),
        Component.For<IGenreRepository>().ImplementedBy<GenreRepository>(),
        Component.For<IGenreManager>().ImplementedBy<GenreManager>(),
        Component.For<ICountryRepository>().ImplementedBy<CountryRepository>(),
        Component.For<ICountryManager>().ImplementedBy<CountryManager>()
      );
      container.Register(Classes.FromAssemblyInThisApplication(GetType().Assembly).BasedOn<Profile>().WithServiceBase());
      container.Register(Component.For<IConfigurationProvider>().UsingFactoryMethod(kernel =>
      {
        return new MapperConfiguration(configuration =>
        {
          kernel.ResolveAll<Profile>().ToList().ForEach(configuration.AddProfile);
        });
      }).LifestyleSingleton());
      container.Register(Component.For<IMapper>().UsingFactoryMethod(kernel =>
                    new Mapper(kernel.Resolve<IConfigurationProvider>(), kernel.Resolve)));
    }
  }
}
