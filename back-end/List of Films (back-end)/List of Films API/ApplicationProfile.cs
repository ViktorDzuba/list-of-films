using AutoMapper;
using ListOfFilms.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace List_of_Films_API
{
  public class ApplicationProfile : Profile
  {
    public ApplicationProfile()
    {
      CreateMap<Film, FilmViewModel>();
    }
  }
}
