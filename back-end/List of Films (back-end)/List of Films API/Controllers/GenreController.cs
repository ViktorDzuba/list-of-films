using ListOfFilms.Business.Interfaces;
using ListOfFilms.Entities;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace List_of_Films_API.Controllers
{
  [RoutePrefix("genres")]
  public class GenreController : ApiController
  {
    private readonly IGenreManager _genreManager;

    public GenreController(IGenreManager genreManager)
    {
      _genreManager = genreManager;
    }

    [HttpGet]
    [Route("")]
    public IEnumerable<Genre> GetAllGenres()
    {
      return _genreManager.GetAllGenres();
    }

    [HttpDelete]
    [Route("{GenreId}")]
    public bool DeleteGenre(int GenreId)
    {
      return _genreManager.DeleteGenre(GenreId);
    }

    [HttpPost]
    [Route("")]
    public object AddNewGenre([FromBody] Genre genre)
    {
      return _genreManager.AddNewGenre(genre);
    }
  }
}
