using ListOfFilms.Business.Interfaces;
using ListOfFilms.Entities;
using System.Collections.Generic;
using System.Web.Http;

namespace List_of_Films_API.Controllers
{
  [RoutePrefix("countries")]
  public class CountryController : ApiController
  {
    private readonly ICountryManager _countryManager;

    public CountryController(ICountryManager countryManager)
    {
      _countryManager = countryManager;
    }

    [HttpGet]
    [Route("")]
    public IEnumerable<Country> GetAllCountries()
    {
      return _countryManager.GetAllCountries();
    }
  }
}
