using ListOfFilms.Business.Interfaces;
using ListOfFilms.Entities;
using System.Collections.Generic;
using System.Web.Http;

namespace List_of_Films_API.Controllers
{
  [RoutePrefix("films")]
  public class FilmController : ApiController
  {
    private readonly IFilmManager _filmManager;
    public FilmController(IFilmManager filmManager)
    {
      _filmManager = filmManager;
    }

    [HttpGet]
    [Route("")]
    public IEnumerable<FilmViewModel> GetAllFilms()
    {
      return _filmManager.GetAllFilms();
    }

    [HttpPost]
    [Route("")]
    public bool PostNewFilm([FromBody] Film film)
    {
      //var body = new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();
      if (ModelState.IsValid)
      {
        return _filmManager.AddFilm(film);
      }
      return false;
    }

    [HttpGet]
    [Route("{Id}")]
    public FilmViewModel GetFilm(int Id)
    {
      return _filmManager.GetFilmById(Id);
    }

    [HttpPut]
    [Route("{Id}")]
    public bool PutFilm([FromBody] Film film)
    {
      if (this.ModelState.IsValid)
      {
        return _filmManager.UpdateFilm(film);
      }
      return false;
    }

    [HttpDelete]
    [Route("{Id}")]
    public void Delete(int Id)
    {
      _filmManager.DeleteFilm(Id);
    }
  }
}
