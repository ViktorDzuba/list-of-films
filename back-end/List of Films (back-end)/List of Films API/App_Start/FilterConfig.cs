﻿using System.Web;
using System.Web.Mvc;

namespace List_of_Films_API
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new HandleErrorAttribute());
    }
  }
}
