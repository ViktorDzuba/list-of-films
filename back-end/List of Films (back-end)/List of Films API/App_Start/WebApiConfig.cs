using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace List_of_Films_API
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      //var cors = new EnableCorsAttribute("http://localhost:8080", "*", "*");
      //config.EnableCors(cors);
      //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
      
      var formatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
        formatter.SerializerSettings = new JsonSerializerSettings
        {
          Formatting = Formatting.Indented,
          TypeNameHandling = TypeNameHandling.Objects,
          ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

      // Конфигурация и службы веб-API
      // Маршруты веб-API
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );
    }
  }
}
