using ListOfFilms.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ListOfFilms.Business.Interfaces
{
  public interface ICountryManager
  {
    IEnumerable<Country> GetAllCountries();
  }
}
