using ListOfFilms.Entities;
using System.Collections.Generic;

namespace ListOfFilms.Business.Interfaces
{
  public interface IFilmManager
  {
    bool AddFilm(Film film);
    bool UpdateFilm(Film film);
    bool DeleteFilm(int Id);
    IEnumerable<FilmViewModel> GetAllFilms();
    FilmViewModel GetFilmById(int Id);
  }
}
