using ListOfFilms.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ListOfFilms.Business.Interfaces
{
  public interface IGenreManager
  {
    IEnumerable<Genre> GetAllGenres();
    bool DeleteGenre(int GenreId);
    object AddNewGenre(Genre genre);
  }
}
