using Dapper;
using ListOfFilms.Entities;
using ListOfFilms.Repository.Interfaces;
using System;
using System.Collections.Generic;
using static System.Data.CommandType;

namespace ListOfFilms.Repository
{
  public class CountryRepository : RepositoryBase, ICountryRepository
  {   

    public IEnumerable<Country> GetAllCountries()
    {
      try
      {
        using(var con = GetConnection())
        {
          IEnumerable<Country> listCoontries = SqlMapper.Query<Country>(con, "GetAllCountries", commandType: StoredProcedure);
          return listCoontries;
        }
      }
      catch(Exception ex)
      {
        throw new Exception("GetAllCountries exception", ex);
      }
    }
  }
}
