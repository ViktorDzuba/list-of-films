using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace ListOfFilms.Repository.Extensions
{
  public static class DapperExtensions
  {
    public static DataTable ToIntArrayTypeDataTable(this IEnumerable<int> instance)
    {
      var dt = new DataTable("dbo.GenresIds");
      dt.Columns.Add("GenreId", typeof(int));

      if (instance != null)
      {
        instance.ToList().ForEach(id => dt.Rows.Add(id));
      }

      return dt;
    }
  }
}
