using Dapper;
using ListOfFilms.Entities;
using ListOfFilms.Repository.Extensions;
using ListOfFilms.Repository.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static System.Data.CommandType;

namespace ListOfFilms.Repository
{
  public class FilmRepository : RepositoryBase, IFilmRepository
  {
    public class JsonTypeHandler : SqlMapper.ITypeHandler
    {
      public void SetValue(IDbDataParameter parameter, object value)
      {
        parameter.Value = JsonConvert.SerializeObject(value);
      }

      public object Parse(Type destinationType, object value)
      {
        return JsonConvert.DeserializeObject(value as string, destinationType);
      }
    }

    public bool AddFilm(Film film)
    {
      try
      {
        using (var con = GetConnection())
        {
          con.Execute("AddFilm", new {
            Title = film.Title,
            Year = film.Year,
            Country = film.CountryId,
            Producer = film.Producer,
            MainRoles = film.MainRoles,
            Description = film.Description,
            GenresIds = film.Genres.ToIntArrayTypeDataTable()
          }, commandType: StoredProcedure); ;
          return true;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("AddFilm exception",ex);
      }
    }
    public bool DeleteFilm(int Id)
    {
      try
      {
        using (var con = GetConnection())
        { 
          con.Execute("DeleteFilm", new { Id }, commandType: StoredProcedure);

          return true;
        }
      }
      catch(Exception ex)
      {
        throw new Exception("DeleteFilm exception",ex);
      }
      
    }
    public IEnumerable<Film> GetAllFilms()
    {
      try
      {
        using (var con = GetConnection())
        {
          SqlMapper.AddTypeHandler(typeof(IEnumerable<int>), new JsonTypeHandler());
          IEnumerable<Film> filmList = SqlMapper.Query<Film>(con, "GetAllFilms", commandType: StoredProcedure);

          return filmList;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("GetAllFilms exeption", ex);
      }
    }
    public Film GetFilmById(int Id)
    {
      try
      {
        using (var con = GetConnection())
        {
          return SqlMapper.Query<Film>(con, "GetFilmById", new { Id }, commandType: StoredProcedure).FirstOrDefault();
        }
      }
      catch (Exception ex)
      {
        throw new Exception("GetFilmById exception", ex);
      }
    }
    public bool UpdateFilm(Film film)
    { 
      try
      {
        using (var con = GetConnection())
        { 
          SqlMapper.Execute(con, "UpdateFilm", new
          {
            Id = film.Id,
            Title = film.Title,
            Year = film.Year,
            Country = film.CountryId,
            Producer = film.Producer,
            MainRoles = film.MainRoles,
            Description = film.Description,
            GenresIds = film.Genres.ToIntArrayTypeDataTable()
          },
          commandType: StoredProcedure);

          return true;
        }
      }
      catch (Exception ex)
      {
        throw new Exception("UpdateFilm exception",ex);
      }
    }
  }
}
