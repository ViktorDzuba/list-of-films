using Dapper;
using ListOfFilms.Entities;
using ListOfFilms.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using static System.Data.CommandType;

namespace ListOfFilms.Repository
{
  public class GenreRepository : RepositoryBase, IGenreRepository
  {
    public IEnumerable<Genre> GetAllGenres()
    {
      try
      {
        using(var con = GetConnection())
        {
          IEnumerable<Genre> genresList = SqlMapper.Query<Genre>(con, "GetAllgenres", commandType: StoredProcedure);
          return genresList;
        }
      }
      catch(Exception ex)
      {
        throw new Exception("GetAllgenres exception", ex);
      }
    }

    public bool DeleteGenre(int GenreId)
    {
      try
      {
        using (var con = GetConnection())
        {
          con.Execute("DeleteGenre", new { GenreId }, commandType: StoredProcedure);
          return true;
        }
      }
      catch(Exception ex)
      {
        throw new Exception("DeleteGenre exception", ex);
      }
    }

    public object AddNewGenre(Genre genre)
    {
      try
      {
        using(var con = GetConnection())
        {
          return con.Query<Genre>("Addgenre", new { NameGenre = genre.NameGenre }, commandType: StoredProcedure);
        }
      }
      catch(Exception ex)
      {
        throw new Exception("AddGenre exception", ex);
      }
    }
  }
}
