﻿using System.Configuration;
using System.Data.SqlClient;

namespace ListOfFilms.Repository
{
  public abstract class RepositoryBase
  {    protected SqlConnection GetConnection()
    {
      string constr = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;
      return new SqlConnection(constr);
    }
  }
}
