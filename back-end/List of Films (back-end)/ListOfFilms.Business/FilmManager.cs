using AutoMapper;
using ListOfFilms.Business.Interfaces;
using ListOfFilms.Entities;
using ListOfFilms.Repository.Interfaces;
using System.Collections.Generic;

namespace ListOfFilms.Business
{
  public class FilmManager : IFilmManager
  {
    private readonly IFilmRepository _filmRepository;
    private readonly IMapper _mapper;

    public FilmManager(IFilmRepository filmRepository, IMapper mapper)
    {
      _filmRepository = filmRepository;
      _mapper = mapper;
    }
    public bool AddFilm(Film film)
    {
      return _filmRepository.AddFilm(film);
    }
    public bool DeleteFilm(int Id)
    {
      return _filmRepository.DeleteFilm(Id);
    }
    public IEnumerable<FilmViewModel> GetAllFilms()
    {
      var films = _filmRepository.GetAllFilms();
      var mapper = _mapper.Map<IEnumerable<Film>, IEnumerable<FilmViewModel>>(films);
      return mapper;
    }
    public FilmViewModel GetFilmById(int Id)
    {
      var film = _filmRepository.GetFilmById(Id);
      var mapper = _mapper.Map<FilmViewModel>(film);
      return mapper;
    }
    public bool UpdateFilm(Film film)
    {
      return _filmRepository.UpdateFilm(film);
    }
  }
}
