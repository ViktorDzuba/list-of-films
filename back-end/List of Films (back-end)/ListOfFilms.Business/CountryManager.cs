using ListOfFilms.Business.Interfaces;
using ListOfFilms.Entities;
using ListOfFilms.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ListOfFilms.Business
{
  public class CountryManager : ICountryManager
  {
    private readonly ICountryRepository _countryRepository;

    public CountryManager(ICountryRepository countryRepository)
    {
      _countryRepository = countryRepository;
    }

    public IEnumerable<Country> GetAllCountries()
    {
      return _countryRepository.GetAllCountries();
    }
  }
}
