using ListOfFilms.Business.Interfaces;
using ListOfFilms.Entities;
using ListOfFilms.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ListOfFilms.Business
{
  public class GenreManager : IGenreManager
  {
    private readonly IGenreRepository _genreRepository;

    public GenreManager(IGenreRepository genreRepository)
    {
      _genreRepository = genreRepository;
    }

    public object AddNewGenre(Genre genre)
    {
      return _genreRepository.AddNewGenre(genre);
    }

    public IEnumerable<Genre> GetAllGenres()
    {
      return _genreRepository.GetAllGenres();
    }

    public bool DeleteGenre(int GenreId)
    {
      return _genreRepository.DeleteGenre(GenreId);
    }
  }
}
