using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ListOfFilms.Entities
{
  public class Country
  {
    public Country() { }
    [Display(Name = "CountryId")]
    public int CountryId { get; set; }
    [Required(ErrorMessage = "NameCountry is required")]
    public string NameCountry { get; set; }
  }
}
