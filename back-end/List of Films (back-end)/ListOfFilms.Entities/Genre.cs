using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ListOfFilms.Entities
{
  public class Genre
  {
    public Genre() { }
    [Display(Name = "GenreId")]
    public int GenreId { get; set; }
    [Required(ErrorMessage = "NameGenre is required.")]
    public string NameGenre { get; set; }
  }
}
