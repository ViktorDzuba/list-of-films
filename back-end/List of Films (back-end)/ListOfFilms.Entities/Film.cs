using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ListOfFilms.Entities
{
  public class Film
  {
    public Film() { }
    [Display(Name = "Id")]
    public int Id { get; set; }
    [Required]
    [MinLength(2)]
    public string Title { get; set; }
    [Required]
    [Range(1000, 2020, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
    public int Year { get; set; }
    [Required]
    public int CountryId { get; set; }
    public IEnumerable<int> Genres { get; set; }
    [Required]
    public string Producer { get; set; }
    [Required]
    public string MainRoles { get; set; }
    public string Description { get; set; }
  }
}
