﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateFilm]
	-- Add the parameters for the stored procedure
	@Id int,
	@Title nvarchar(50),
	@Year int,
	@Country int,
	@Producer nvarchar(50),
	@MainRoles nvarchar(MAX),
	@Description nvarchar(MAX),
	@GenresIds [dbo].[GenresIds] READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update Films set Title=@Title, Year=@Year,Country=@Country, Producer=@Producer, MainRoles=@MainRoles, Description=@Description
	where Id=@Id

	delete from GenresForFilms
	where [FilmId] = @Id

	insert into GenresForFilms ([FilmId], [GenreId])
	select
		@Id
		,g.GenreId
	from @GenresIds g
END