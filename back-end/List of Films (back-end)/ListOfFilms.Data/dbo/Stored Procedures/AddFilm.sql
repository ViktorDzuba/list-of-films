﻿/*
declare @tGenereIds [dbo].[GenresIds]
insert into @tGenereIds
select [GenreId] from [dbo].[Genres]

declare @tFilms [dbo].[GenresIds]

insert into @tFilms
exec [dbo].[AddFilm]
	@Title			 = 'Hi there!'
	,@Year			 = 2
	,@Country		 = 2
	,@Producer		 = 'Hi there!'
	,@MainRoles		 = 'Hi there!'
	,@Description	 = 'Hi there!'
	,@GenresIds		 = @tGenereIds

declare @filmId int = (select top(1) [GenreId] from @tFilms)

select * from dbo.Films 
where [Id] = @filmId

*/
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddFilm]
	-- Add the parameters for the stored procedure here
	--@Id bigint,
  @Title		nvarchar(50),
  @Year			int,
  @Country		int,
  @Producer		nvarchar(50),
  @MainRoles	nvarchar(MAX),
  @Description	nvarchar(MAX),
  @GenresIds	[dbo].[GenresIds] READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Films (Title, Year, Country, Producer, MainRoles, [Description])
	values (@Title, @Year, @Country, @Producer, @MainRoles, @Description)

	declare @filmId int = SCOPE_IDENTITY()

	insert into GenresForFilms ([FilmId], [GenreId])
	select
		@filmId
		,g.GenreId
	from @GenresIds g

	select @filmId
END
