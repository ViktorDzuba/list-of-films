﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddGenre] 
	-- Add the parameters for the stored procedure here
	@NameGenre nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Genres (NameGenre)
	VALUES (@NameGenre)

	SELECT * FROM Genres WHERE GenreId = SCOPE_IDENTITY()
END
