﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec [dbo].[GetAllFilms]
CREATE PROCEDURE [dbo].[GetAllFilms]
	-- Add the parameters for the stored procedure 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Films f
	left join Countries c on f.Country = c.CountryId
	left join ( 
		SELECT
			gf.FilmId,
			'[' + STRING_AGG(g.GenreId, ',')+']' as Genres
		FROM GenresForFilms gf
		left join Genres g
			ON gf.GenreId = g.GenreId
		GROUP By gf.FilmId
	) ge 
	ON f.Id = ge.FilmId
END
