﻿CREATE TABLE [dbo].[GenresForFilms] (
    [FilmId]  INT NOT NULL,
    [GenreId] INT NOT NULL,
    CONSTRAINT [FK_GenresForFilms_Films] FOREIGN KEY ([FilmId]) REFERENCES [dbo].[Films] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_GenresForFilms_Genres] FOREIGN KEY ([GenreId]) REFERENCES [dbo].[Genres] ([GenreId]) ON DELETE CASCADE ON UPDATE CASCADE
);



