﻿CREATE TABLE [dbo].[Films] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (50)  NOT NULL,
    [Year]        INT            NOT NULL,
    [Country]     INT            NOT NULL,
    [Producer]    NVARCHAR (50)  NOT NULL,
    [MainRoles]   NVARCHAR (MAX) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Films] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Films_Countries] FOREIGN KEY ([Country]) REFERENCES [dbo].[Countries] ([CountryId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
ALTER TABLE [dbo].[Films] NOCHECK CONSTRAINT [FK_Films_Countries];
















GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT TAKE OWNERSHIP
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT REFERENCES
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT CONTROL
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];


GO
GRANT ALTER
    ON OBJECT::[dbo].[Films] TO [guest]
    AS [dbo];

