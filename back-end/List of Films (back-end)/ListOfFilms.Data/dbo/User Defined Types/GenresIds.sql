﻿CREATE TYPE [dbo].[GenresIds] AS TABLE (
    [GenreId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([GenreId] ASC));

