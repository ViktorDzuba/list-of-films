using ListOfFilms.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ListOfFilms.Repository.Interfaces
{
  public interface IGenreRepository
  {
    IEnumerable<Genre> GetAllGenres();
    bool DeleteGenre(int GenreId);
    object AddNewGenre(Genre genre);
  }
}
