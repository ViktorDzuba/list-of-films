using System;
using System.Collections.Generic;
using System.Text;
using ListOfFilms.Entities;

namespace ListOfFilms.Repository.Interfaces
{
  public interface IFilmRepository
  {
    bool AddFilm(Film film);
    bool UpdateFilm(Film film);
    bool DeleteFilm(int Id);
    IEnumerable<Film> GetAllFilms();
    Film GetFilmById(int Id);
  }
}
