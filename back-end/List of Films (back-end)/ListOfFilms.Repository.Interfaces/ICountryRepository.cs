using ListOfFilms.Entities;
using System.Collections.Generic;

namespace ListOfFilms.Repository.Interfaces
{
  public interface ICountryRepository
  {
    IEnumerable<Country> GetAllCountries();
  }
}
