import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.vue'
import EditorFilm from '@/components/EditorFilm.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/editorFilm',
      name: 'editorFilm',
      component: EditorFilm,
      props: true,
      meta: {
        hideHeader: true
      }
    }
  ]
})

router.afterEach((to, from) => {
  const navbar = document.querySelector('.navbar')
  const short = navbar && navbar.classList.contains('short')
  if (navbar && short !== to.meta.hideHeader) {
    document.querySelector('.navbar').classList.toggle('short')
  }
})

export default router
