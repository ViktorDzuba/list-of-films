import Axios from 'axios'

const RESOURCE_NAME = '/genres'

export default {
  getAllGenres () {
    return Axios.get(RESOURCE_NAME).then(res => res.data)
  },

  createGenre (data) {
    return Axios.post(RESOURCE_NAME, data)
  },

  deleteGenre (id) {
    return Axios.delete(`${RESOURCE_NAME}/${id}`)
  }
}
