import Axios from 'axios'

const RESOURCE_NAME = '/countries'

export default {
  getAllCountries () {
    return Axios.get(RESOURCE_NAME).then(res => res.data)
  }
}
