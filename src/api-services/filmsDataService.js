import Axios from 'axios'

const RESOURCE_NAME = '/films'

export default {
  getAllFilms () {
    return Axios.get(RESOURCE_NAME).then(res => res.data)
  },

  getFilm (id) {
    return Axios.get(`${RESOURCE_NAME}/${id}`)
  },

  create (data) {
    return Axios.post(RESOURCE_NAME, data)
  },

  update (data) {
    return Axios.put(`${RESOURCE_NAME}/${data.id}`, data)
  },

  delete (id) {
    return Axios.delete(`${RESOURCE_NAME}/${id}`)
  }
}
