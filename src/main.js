// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Axios from 'axios'
import Vuelidate from 'vuelidate'

Vue.config.productionTip = false

Vue.use(Vuelidate)

Axios.defaults.baseURL = '/api'

Vue.filter('getCountryName', id => {
  const country = store.getters.countries.find(c => c.countryId === id)
  return country ? country.nameCountry : 'Test'
})
Vue.filter('getGenresNames', (ids) => ids && ids.length ? store.getters.genres.filter(g => ids.includes(g.genreId)).map(g => g.nameGenre).join(', ') : '')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
