import filmsDataService from '../api-services/filmsDataService'
import genresDataService from '../api-services/genresDataService'
import countriesDataService from '../api-services/countriesDataService'

export default {
  state: {
    films: [],
    genres: [],
    countries: [],
    searchText: ''
  },
  mutations: {
    // mutations for films
    updateFilm (state, payload) {
      const exist = state.films.findIndex(film => film.id === payload.id)
      if (exist) {
        state.films.splice(exist, 1, Object.assign({}, exist, payload))
        state.films = [...state.films]
        return
      }
      state.films.push(payload)
    },
    getFilms (state, payload) {
      state.films = payload
    },
    createFilm (state, payload) {
      state.films.push(payload)
    },
    deleteFilm (state, id) {
      state.films = state.films.filter(film => film.id !== id)
    },
    updateSearchText (state, text) {
      state.searchText = text
    },

    // mutations for genres of films
    getAllGenres (state, payload) {
      state.genres = payload
    },
    createGenre (state, payload) {
      state.genres.push(payload[0])
    },
    deleteGenre (state, id) {
      state.genres = state.genres.filter(genre => genre.genreId !== id)
    },

    // mutations for countries of films
    getAllCountries (state, payload) {
      state.countries = payload
    }
  },
  actions: {
    // actions for films
    async updateFilm ({commit}, payload) {
      try {
        await filmsDataService.update(payload)
        commit('updateFilm', payload)
      } catch (error) {
        console.log(error)
      }
    },
    async getFilms ({commit}) {
      commit('getFilms', await filmsDataService.getAllFilms())
    },
    async createFilm ({commit}, payload) {
      const res = await filmsDataService.create(payload)
      if (res.status > 200 && res.status < 300) {
        commit('createFilm', payload)
      }
    },
    async deleteFilm ({commit}, id) {
      await filmsDataService.delete(id)
      commit('deleteFilm', id)
    },

    // actions for genres of films
    async getAllGenres ({commit}) {
      commit('getAllGenres', await genresDataService.getAllGenres())
    },
    async createGenre ({commit}, payload) {
      const res = await genresDataService.createGenre(payload)
      if (res.status >= 200 && res.status < 300) {
        commit('createGenre', res.data)
      }
    },
    async deleteGenre ({commit}, id) {
      await genresDataService.deleteGenre(id)
      commit('deleteGenre', id)
    },

    // actions for countries of films
    async getAllCountries ({commit, state}) {
      if (!state.countries.length) {
        commit('getAllCountries', await countriesDataService.getAllCountries())
      }
    }
  },
  getters: {
    films (state) {
      return state.films
    },
    searchText (state) {
      return state.searchText
    },
    genres (state) {
      return state.genres
    },
    countries (state) {
      return state.countries
    }
  }
}
